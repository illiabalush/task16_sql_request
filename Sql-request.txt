1.
1. select pr.maker, pr.type from pc p
inner join product pr
on pr.model = p.model
order by maker desc;
2. select model, ram, screen, price from laptop
where price < 1000
order by price desc
3. select * from printer
where color = 'y'
order by price desc
4. select model, speed, hd, cd, price from pc
where cd in ('12x', '24x') and price < 600
order by speed desc
5. select s.name, s.class from ships s
inner join classes c
on c.class = s.class and c.type = 'bc'
order by name desc 
6. select * from pc p
where p.speed > 500 and p.price < 800
order by price desc
7. select * from printer p
where p.type not in ('matrix') and p.price < 300
 order by type desc
8. select model, speed from pc
where price between 400 and 600
order by hd 
9. select p.model, p.speed, p.hd from pc p
inner join product pr
on pr.model = p.model and pr.maker = 'A'
where p.hd in (10, 20)
order by p.speed
10. select model, speed, hd, price from laptop
where screen > 12
order by price desc
11. select model, type, price from printer
 where price < 300
order by type desc
12. select model, ram, price from laptop
 where ram = 64 
order by screen
13. select model, ram, price from laptop 
where ram > 64 
order by hd
14. select model, speed, price from pc
where speed between 500 and 750
order by hd desc
15. select * from outcome_o o
where o.out > 2000
order by o.date desc
16. select * from income_o i
where i.inc between 5000 and 10000
order by i.inc
17. select * from income_o i
where i.point = 1
order by i.inc
18. select * from outcome_o o
where o.point = 2
order by o.out
19. select * from ships s
inner join classes c
on c.class = s.class and c.country = 'Japan'
order by c.type desc
20. select s.name, s.launched from ships s
inner join classes c
on c.class = s.class and s.launched between 1920 and 1942
order by launched desc
21. select ship, battle, result from ships s
inner join outcomes o
on o.ship = s.name and o.battle = 'Guadalcanal' and result in ('damaged', 'OK')
order by o.ship desc
22. select o.ship, o.battle, o.result from outcomes o
where o.result in ('sunk')
order by o.ship desc
23. select class, displacement from classes
where displacement > 40000
order by type
24. select trip_no, town_from, town_to from trip
where town_from = 'London'
order by time_out
25. select trip_no, plane, town_from, town_to from trip
where plane = 'TU-134'
order by time_out
26. select trip_no, plane, town_from, town_to from trip
where plane not in ('TU-134')
order by plane
27. select trip_no, town_from, town_to from trip
where town_from not in ('Rostov')
order by plane

2.
1. select model from pc
where model rlike ".*1+.*1+"
4. select name from ships
where name rlike '^W.*n$'