CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`group` (
  `id` INT NOT NULL,
  `group_name` VARCHAR(15) NOT NULL,
  `group_number` VARCHAR(15) NOT NULL,
  `year_of_arrival` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`district`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`district` (
  `id` INT NOT NULL,
  `district` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`City`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`City` (
  `id` INT NOT NULL,
  `city` VARCHAR(35) NOT NULL,
  `district_id` INT NOT NULL,
  PRIMARY KEY (`id`, `district_id`),
  INDEX `fk_City_district1_idx` (`district_id` ASC),
  CONSTRAINT `fk_City_district1`
    FOREIGN KEY (`district_id`)
    REFERENCES `mydb`.`district` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`secondary_education`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`secondary_education` (
  `id` INT NOT NULL,
  `institution_name` VARCHAR(35) NOT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `director_name` VARCHAR(25) NOT NULL,
  `director_surname` VARCHAR(25) NOT NULL,
  `City_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_secondary_education_City1_idx` (`City_id` ASC),
  CONSTRAINT `fk_secondary_education_City1`
    FOREIGN KEY (`City_id`)
    REFERENCES `mydb`.`City` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student` (
  `id` INT NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  `surname` VARCHAR(25) NOT NULL,
  `lastname` VARCHAR(25) NOT NULL,
  `general_rate` DECIMAL NOT NULL,
  `date_of_arrival` DATE NOT NULL,
  `student_id` VARCHAR(25) NOT NULL,
  `email` VARCHAR(35) NULL,
  `group_id` INT NOT NULL,
  `City_id` INT NOT NULL,
  `secondary_education_id` INT NOT NULL,
  PRIMARY KEY (`id`, `City_id`, `secondary_education_id`),
  INDEX `fk_student_group_idx` (`group_id` ASC),
  INDEX `fk_student_City1_idx` (`City_id` ASC),
  INDEX `fk_student_secondary_education1_idx` (`secondary_education_id` ASC),
  CONSTRAINT `fk_student_group`
    FOREIGN KEY (`group_id`)
    REFERENCES `mydb`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_City1`
    FOREIGN KEY (`City_id`)
    REFERENCES `mydb`.`City` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_secondary_education1`
    FOREIGN KEY (`secondary_education_id`)
    REFERENCES `mydb`.`secondary_education` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`arrears`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`arrears` (
  `id` INT NOT NULL,
  `subject_name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`intermediate_student_arreats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`intermediate_student_arreats` (
  `id` INT NOT NULL,
  `arrears_id` INT NOT NULL,
  `student_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_arrears_has_student_student1_idx` (`student_id` ASC),
  INDEX `fk_arrears_has_student_arrears1_idx` (`arrears_id` ASC),
  CONSTRAINT `fk_arrears_has_student_arrears1`
    FOREIGN KEY (`arrears_id`)
    REFERENCES `mydb`.`arrears` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_arrears_has_student_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `mydb`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
