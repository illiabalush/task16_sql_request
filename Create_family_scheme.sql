-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema task_tree
-- -----------------------------------------------------
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`sex_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`sex_table` (
  `id` INT NOT NULL,
  `sex` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_tree` (
  `id` INT NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  `surname` VARCHAR(25) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `place_of_birth` VARCHAR(25) NOT NULL,
  `date_of_death` DATE NULL,
  `place_of_death` VARCHAR(25) NULL,
  `credit_card_number` VARCHAR(15) NULL,
  `sex_table_id` INT NOT NULL,
  `family_tree_id` INT NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`),
  INDEX `fk_family_tree_sex_table1_idx` (`sex_table_id` ASC),
  INDEX `fk_family_tree_family_tree1_idx` (`family_tree_id` ASC),
  CONSTRAINT `fk_family_tree_sex_table1`
    FOREIGN KEY (`sex_table_id`)
    REFERENCES `mydb`.`sex_table` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `mydb`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_satellite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_satellite` (
  `id` INT NOT NULL,
  `name` VARCHAR(25) NOT NULL,
  `surname` VARCHAR(25) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `place_of_birth` VARCHAR(20) NOT NULL,
  `date_of_death` DATE NULL,
  `place_of_death` VARCHAR(25) NULL,
  `date_of_marriage` DATE NULL,
  `sex_table_id` INT NOT NULL,
  `family_tree_id` INT NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`),
  INDEX `fk_family_satellite_sex_table_idx` (`sex_table_id` ASC),
  INDEX `fk_family_satellite_family_tree1_idx` (`family_tree_id` ASC),
  CONSTRAINT `fk_family_satellite_sex_table`
    FOREIGN KEY (`sex_table_id`)
    REFERENCES `mydb`.`sex_table` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_satellite_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `mydb`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_value` (
  `id` INT NOT NULL,
  `value_name` VARCHAR(45) NOT NULL,
  `value` DECIMAL NULL,
  `max_value` DECIMAL NOT NULL,
  `min_value` DECIMAL NOT NULL,
  `code_value` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_tree_has_family_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_tree_has_family_value` (
  `id` VARCHAR(45) NOT NULL,
  `family_tree_id` INT NOT NULL,
  `family_value_id` INT NOT NULL,
  INDEX `fk_family_tree_has_family_value_family_value1_idx` (`family_value_id` ASC),
  INDEX `fk_family_tree_has_family_value_family_tree1_idx` (`family_tree_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_family_tree_has_family_value_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `mydb`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_has_family_value_family_value1`
    FOREIGN KEY (`family_value_id`)
    REFERENCES `mydb`.`family_value` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
